import React from 'react';
import ScrollAnimation from "react-animate-on-scroll";
import "../css/styleContact.css";

/* --------------------- ICONS ---------------------- */
import { FaPhoneAlt } from "react-icons/fa";
import { MdEmail } from "react-icons/md";
import { MdLocationOn } from "react-icons/md";

const Contact = () => {
  return (
    <>
      <div className = "page">
        <div className = "pageposition">
        <ScrollAnimation animateIn="fadeIn">
            <h1 className="aboutheading1">CONTACT ME</h1>
          </ScrollAnimation>

          <div className="progress prohor">
            <div className="progress-bar bg-blue progress-bar-striped horizontalrow"></div>
          </div>
        </div>
        <div className=" pageposition pb-5">
          <div className="row">
            <div className="col-12 col-md-6 pl-2 pr-2">
              {/* <!-- Default form contact --> */}
              <iframe title="locationframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14242.014044436684!2d83.36667836977536!3d26.823933099999987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39914583da5d2ee7%3A0x94ecb0669c33bd92!2sKendriya%20Vidyalaya%20No.%202%2C%20FCI!5e0!3m2!1sen!2sin!4v1600306933908!5m2!1sen!2sin" className="text-center contactfrom p-4 ml-3 mr-5"></iframe>
            </div>


            <div className="col-12 col-md-6 pl-0 pr-0">
                <div className="card contactcard m-4">
                  <div className="card-body">	
                    <FaPhoneAlt className="icons" />
                    <p className="card-text">Phone </p>
                    <br />
                    <h5 className="cardelement">
                      <a href="tel: +916388541748">+91 6388541748</a>
                    </h5>
                    <h5 className="cardelement">
                      <a href="tel: +918299639067">+91 8299639067</a>
                    </h5>
                  </div>
                </div>
                <div className="card contactcard m-4">
                  <div className="card-body">
                    <MdEmail className="icons" />
                    <p className="card-text">Email </p>
                    <br />
                    <h5 className="cardelement">
                      <a href="mailto: deepdeo0786@gmail.com">
                        deepdeo0786@gmail.com
                      </a>
                    </h5>
                    <h5 className="cardelement">
                      <a href="mailto: bit17ec092@bit.ac.in">
                        bit17ec092@bit.ac.in
                      </a>
                    </h5>
                  </div>
                </div>
                <div className="card contactcard m-4 ">
                  <div className="card-body">
                    <MdLocationOn className="icons" />
                    <p className="card-text">Address</p>
                    <br />
                    <h5 className="cardelement">
                      Manbela Tola Bangla, Near FCI Colony Gorakhpur,UP
                    </h5>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Contact

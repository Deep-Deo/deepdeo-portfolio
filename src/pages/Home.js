import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import "../css/styleHome.css";

/* -------------- ICONS ------------------ */
import { FaFacebook } from "react-icons/fa";
import { AiFillInstagram } from "react-icons/ai";
import { TiSocialYoutubeCircular } from "react-icons/ti";
import { IoLogoLinkedin } from "react-icons/io";
import { AiFillTwitterCircle } from "react-icons/ai";

const Home = () => {
  return (
    <>
      <div className="page">
        <div className="pageposition center maincontent">
          <center>
            <div className="intro">
              <ScrollAnimation
                animateIn="bounceInRight"
              >
                <h1>
                  Hi, I am <span>Deep Deo</span>
                </h1>
              </ScrollAnimation>
            </div>
            <p className="introP">
              I am a Full Stack Web Developer. I have done specialization in
              Full-Stack Web Development with React. I am also a founder of{" "}
              <a
                href="https://www.youtube.com/channel/UCyITNfrtkcOtKWO5cGoqhwA?view_as=subscriber"
                target="_blank"
                rel="noopener noreferrer"
              >
                {" "}
                Simple&nbsp;Pro&nbsp;Project (YouTube Channel)
              </a>
              . I'm currently pursuing my{" "}
              <span id="MCA">
                &quot;<strong>B.tech</strong>&nbsp;(Electronic&nbsp;and&nbsp;Communication)
              </span>{" "}
              from{" "}
              <span className="UOH"><a href = "http://www.bit.ac.in/" target="_blank" rel="noopener noreferrer"> 
                Buddha&nbsp;Institute&nbsp;of&nbsp;Technology</a>
              </span>
              , Gida Gorakhpur, UP.{" "}
            </p>
            <ScrollAnimation animateIn="bounce" animateOut="bounce">
              <div className="socialicon">
                <a
                  href="https://www.facebook.com/profile.php?id=100030746234063"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <span>
                    <FaFacebook />
                  </span>
                </a>
                <a
                  href="https://www.instagram.com/deepdeo0786/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <span>
                    <AiFillInstagram />
                  </span>
                </a>
                <a
                  href="https://www.youtube.com/channel/UCyITNfrtkcOtKWO5cGoqhwA?view_as=subscriber"
                  target="blank"
                  rel="noopener noreferrer"
                >
                  <span>
                    <TiSocialYoutubeCircular />
                  </span>
                </a>
                <a
                  href="https://www.linkedin.com/in/deep-deo-685b701b2/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <span>
                    <IoLogoLinkedin />
                  </span>
                </a>
                <a
                  href="https:www.twitter.com"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <span>
                    <AiFillTwitterCircle />
                  </span>
                </a>
              </div>
            </ScrollAnimation>
          </center>
        </div>
      </div>
    </>
  );
};

export default Home;

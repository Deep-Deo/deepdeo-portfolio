export default[
  {
    "Id": 1,
    "Name": "Nielit Solutions Website",
    "Technology": [{ "id":1,"Tech" :"ReactJS" }, { "id":2,"Tech" : "ExpressJS" }, {"id":3, "Tech": "Firebase"} ],
    "Gitlink": "https://Deep-Deo@bitbucket.org/Deep-Deo/nielit-solutions.git",
    "Type": "Web Development",
    "Link": "#"
  },
  {
    "Id": 2,
    "Name": "Portfolio Website",
    "Technology": [{ "id":1,"Tech" :"ReactJS" }, { "id":2,"Tech" : "ExpressJS" }, {"id":3, "Tech": "Firebase"} ],
    "Gitlink": "https://Deep-Deo@bitbucket.org/Deep-Deo/deepdeo-portfolio.git",
    "Type": "Web Development",
    "Link": "#"
  },
  {
    "Id": 3,
    "Name": "Flutter App",
    "Technology": [{ "id":1,"Tech" :"Flutter" }, { "id": 2,"Tech" : "Firebase" }, {"id" : 3, "Tech" : "Firestore"} ],
    "Gitlink": "https://Deep-Deo@bitbucket.org/Deep-Deo/flutterapp.git",
    "Type": "Application Software",
    "Link": "#"
  },
  {
    "Id": 4,
    "Name": "AI Game 'Stay Home'",
    "Technology": [{ "id":1,"Tech" :"Java" }, { "id":2,"Tech" : "Java Swing" }, {"id": 3, "Tech": "Derby DB"} ],
    "Gitlink": "https://Deep-Deo@bitbucket.org/Deep-Deo/stay-home.git",
    "Type": "Application Software",
    "Link": "#"
  },
  {
    "Id": 5,
    "Name": "MERN Website",
    "Technology": [{ "id":1,"Tech" :"Mongodb" }, { "id":2,"Tech" : "ExpressJS" },{ "id":3,"Tech" :"ReactJS" }, { "id":4,"Tech" : "NodeJS" }  ],
    "Gitlink": "https://Deep-Deo@bitbucket.org/Deep-Deo/mern-stack.git",
    "Type": "Web Development",
    "Link": "#"
  }
]

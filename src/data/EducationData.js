export default[
  {
    "Id": 1,
    "Year": "2021",
    "College":"Dr. A.P.J. Abdul Kalam Technical University",
    "School": "Buddha Institute of Technoloy",
    "Degree":"Bachelor of Technology (ECE)",
    "Score": "SCORE - 67.3",
    "Discription": "The main objective of the BTech (ECE) program is to equip students with necessary core competency to succeed long-term in engineering/ entrepreneurship careers after completing their B.Tech. and are preparing to undertake PG studies and research as career options. As a discipline, ECE focuses on the design of underlying hardware systems. "
  },
  {
    "Id": 2,
    "Year": "2016",
    "College":"Central Board of Secondary Education",
    "School": "Government Senior Seconday School, Mangan North Sikkim",
    "Degree":"AISSCE (12th)",
    "Score": "SCORE - 58.2%",
    "Discription": "Completed with Physics Chemistry Mathematics"
  },
  {
    "Id": 3,
    "Year": "2014",
    "College":"Central Board of Secondary Education",
    "School": "North Sikkim Academy, Mangan North Sikkim",
    "Degree":"AISSE (10th)",
    "Score": "Score - 86 CGPA",
    "Discription": "Mathematics | Science | English | Hindi | Social-Science"
  }
]
              
